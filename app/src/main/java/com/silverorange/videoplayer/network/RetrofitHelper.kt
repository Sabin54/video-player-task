package com.silverorange.videoplayer.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitHelper {
    private var apiInterface: ServerApi ?= null

    @JvmStatic
    val instance: ServerApi
        get() {
            return if (apiInterface == null) {
                val retrofit = Retrofit.Builder()
                    .baseUrl("http://10.0.0.179:4000/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                apiInterface = retrofit.create(ServerApi::class.java)
                apiInterface!!

            } else  {
                apiInterface!!
            }
        }
}
