package com.silverorange.videoplayer.network

import com.silverorange.videoplayer.main_module.module.SampleResponse
import retrofit2.Response
import retrofit2.http.*

interface ServerApi {
        @GET("videos")
        suspend fun getVideoLists(): Response<List<SampleResponse>>
}