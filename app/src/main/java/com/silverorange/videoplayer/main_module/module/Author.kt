package com.silverorange.videoplayer.main_module.module

import com.google.gson.annotations.SerializedName

class Author(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String
)