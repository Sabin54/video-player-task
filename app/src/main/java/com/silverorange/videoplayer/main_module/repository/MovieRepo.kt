package com.silverorange.videoplayer.main_module.repository

import com.silverorange.videoplayer.network.RetrofitHelper

class MovieRepo {
    suspend fun getAllMovies() = RetrofitHelper.instance.getVideoLists()
}