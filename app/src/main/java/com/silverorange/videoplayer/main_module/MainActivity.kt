package com.silverorange.videoplayer.main_module

import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.MotionEvent
import android.view.View.*
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.ui.StyledPlayerView
import com.silverorange.videoplayer.databinding.ActivityMainBinding
import com.silverorange.videoplayer.main_module.module.SampleResponse
import com.silverorange.videoplayer.main_module.viewmodel.MovieVM
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class MainActivity : AppCompatActivity(){
    private val binding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }
    companion object{
        var viewModel: MovieVM? = null
        var responseData  : List<SampleResponse> ?=null
        var currVideo = 0
        private var mHandler: Handler? = null
        private var mRunnable: Runnable? = null
        var player: ExoPlayer ?= null
        var onPause = true
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        initComponents()
    }

    private fun initComponents() {
        initViewModel()
        viewModel!!.getAllVideos()
        initObserver()
        onClickListeners()
        mHandler = Handler(Looper.myLooper()!!)
        mRunnable = Runnable { binding.controllerView.visibility = INVISIBLE }

    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(this)[MovieVM::class.java]
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun initObserver(){
        viewModel!!.movieList.observe(this){
            val dateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            responseData = it.sortedBy {data->
                LocalDate.parse(data.publishedat, dateTimeFormatter)
            }

            loadNewContent(responseData!![0])
        }
        viewModel!!.loading.observe(this){
            if (it){
                if (binding.progressBar.isVisible){
                    binding.progressBar.visibility = GONE
                }
                binding.progressBar.visibility = VISIBLE
            }else{
                binding.progressBar.visibility = GONE
            }
        }
        viewModel!!.errorMessage.observe(this){
            Toast.makeText(this,it,Toast.LENGTH_LONG).show()
        }
    }


    private fun loadNewContent(contentData : SampleResponse){
        onPause = true
        binding.videoData = contentData
        player= ExoPlayer.Builder(this).build()
        val styledPlayerView: StyledPlayerView = binding.videoView
        styledPlayerView.player = player;
        val videouri = Uri.parse(contentData.hlsurl)
        val mediaItem: MediaItem = MediaItem.fromUri(videouri)
        player!!.setMediaItem(mediaItem)
        player!!.prepare()
        binding.btnPlay.visibility = VISIBLE
        binding.btnPause.visibility = INVISIBLE
    }

    private fun onClickListeners(){
        binding.btnPlay.setOnClickListener {
            onPause = false
            player!!.play()
            binding.btnPlay.visibility = INVISIBLE
            binding.btnPause.visibility = VISIBLE
            hideController()
        }

        binding.btnPause.setOnClickListener {
            player!!.pause()
            onPause = true
            binding.btnPlay.visibility = VISIBLE
            binding.btnPause.visibility = INVISIBLE
            binding.controllerView.visibility = VISIBLE
            mHandler!!.removeCallbacks(mRunnable!!)
            hideController()
        }

        binding.btnNext.setOnClickListener {
            currVideo++
            if (currVideo >= responseData!!.size){
                currVideo = responseData!!.size -1
            }else{
                player!!.stop()
                mHandler!!.removeCallbacks(mRunnable!!)
                loadNewContent(responseData!![currVideo])
                hideController()
            }
        }
        binding.btnPrevious.setOnClickListener {
            currVideo--
            if (currVideo<0){
                currVideo = 0
            }else{
                player!!.stop()
                mHandler!!.removeCallbacks(mRunnable!!)
                loadNewContent(responseData!![currVideo])
                hideController()
            }

        }
    }

    private fun hideController(){
        binding.controllerView.visibility = VISIBLE
        if (!onPause){
            mHandler!!.postDelayed(mRunnable!!, 3000)
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        hideController()
        return super.onTouchEvent(event)

    }
}