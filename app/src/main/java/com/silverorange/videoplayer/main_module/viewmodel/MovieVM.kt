package com.silverorange.videoplayer.main_module.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.silverorange.videoplayer.main_module.module.SampleResponse
import com.silverorange.videoplayer.main_module.repository.MovieRepo
import kotlinx.coroutines.*

class MovieVM : ViewModel() {
    private val repository by lazy {
        MovieRepo()
    }

    val errorMessage = MutableLiveData<String>()
    val movieList = MutableLiveData<List<SampleResponse>>()
    val loading = MutableLiveData<Boolean>()

    fun getAllVideos() {
        viewModelScope.launch {
            repository.getAllMovies().let {responseData->
                if (responseData.isSuccessful){
                    movieList.postValue(responseData.body())
                    loading.value = false
                }else{
                    onError("Error : ${responseData.message()} ")
                }
            }
        }
    }
    fun onError(message: String) {
        errorMessage.value = message
        loading.value = false
    }

}