package com.silverorange.videoplayer.main_module.module

import com.google.gson.annotations.SerializedName
class SampleResponse(
    @SerializedName("id")
    val id: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("hlsURL")
    val hlsurl: String,
    @SerializedName("fullURL")
    val fullurl: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("publishedAt")
    val publishedat: String,
    @SerializedName("author")
    val author: Author
)